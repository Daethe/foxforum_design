function showSearch() {
    var hasClass = hasIn('hide', $('.searchBar').attr('class').split(/\s+/));

    if (hasClass) {
        $('.searchBar').removeClass('hide').animate({right: "+=16.6667%"}, 500);
    } else if (!hasClass) {
        $('.searchBar').addClass('hide').animate({right: "-=16.6667%"}, 500);
    }
}
