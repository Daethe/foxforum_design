function showSettings() {
    var hasClass = hasIn('hide', $('#settings').attr('class').split(/\s+/));

    if (hasClass) {
        $('#settings').removeClass('hide').animate({ right: "+=200px"}, 300);
    } else if (!hasClass) {
        $('#settings').addClass('hide').animate({ right: "-=200px"}, 300);
    }
}

function isLogged() {
    var btnHasClass = hasIn('show', $('.infoBar .infoBtn').attr('class').split(/\s+/));
    var detHasClass = hasIn('show', $('.infoBar .infoDet').attr('class').split(/\s+/));

    if (btnHasClass && !detHasClass) {
        $('.infoBar .infoBtn').removeClass('show').addClass('hide');
        $('.infoBar .infoDet').removeClass('hide').addClass('show');
    } else if (!btnHasClass && detHasClass) {
        $('.infoBar .infoDet').removeClass('show').addClass('hide');
        $('.infoBar .infoBtn').removeClass('hide').addClass('show');
    }
}

function withLabel() {
    var hasClass = hasIn('hide', $('.notification .label').attr('class').split(/\s+/));

    if (hasClass)
        $('.notification .label').removeClass('hide');
    else if (!hasClass)
        $('.notification .label').addClass('hide');
}

function sign(data) {
    if (data === 'in') {
        $('#sign .signIn').removeClass('hide').addClass('show');
        $('#sign .signUp').removeClass('show').addClass('hide');
    } else if (data === 'up') {
        $('#sign .signUp').removeClass('hide').addClass('show');
        $('#sign .signIn').removeClass('show').addClass('hide');
    }
}
