function hasIn(className, classList) {
    var has = false;
    for (var i = 0; i < classList.length; i++) {
        if (classList[i] == className)
            has = true;
    }
    return has;
}
