# FoxForum Design

Repository of theme for Telescope app. This repository doesn't use any file of Telescope App but is made to be compatible with this one.

## License

This repository is under MIT License

## ToDo

- responsive
    - tablet integration
    - mobile integration
